# frozen_string_literal: true

require_relative 'base'
require 'mysql2'
require 'open-uri'
require 'httparty'
require 'nokogiri'

class ImportScripts::SocialEngine < ImportScripts::Base

  DB_HOST ||= ENV['DB_HOST']
  DB_NAME ||= ENV['DB_NAME']
  DB_PASS ||= ENV['DB_PASS']
  DB_USER ||= ENV['DB_USER']

  TABLE_PREFIX ||= ENV['TABLE_PREFIX'] || "engine4"

  BATCH_SIZE ||= 100

  AVATAR_DIR = ENV['AVATAR_DIR'] || "/shared/uploads/default"

  def initialize
    super
    @client = Mysql2::Client.new(
      host:     DB_HOST,
      username: DB_USER,
      password: DB_PASS,
      database: DB_NAME,
      reconnect: true
    )
    @skip_updates = true
    SiteSetting.tagging_enabled = true
    SiteSetting.max_tags_per_topic = 10
    SiteSetting.disable_emails = 'yes'
    SiteSetting.newuser_spam_host_threshold = 99
    # => MAYBE these, too, dependin on your rules
    #SiteSetting.body_min_entropy = 0
    #SiteSetting.min_topic_title_length = 5
    #SiteSetting.title_min_entropy = 0
    SiteSetting.allow_uppercase_posts = true
  end

  def execute
    puts "Now starting the SE Import..."
    puts "DB Name: #{DB_NAME}"
    puts "Table Prefix: #{TABLE_PREFIX}"
    puts
    import_users

    disable_users

    add_deleted_users

    import_categories

    import_topics_and_posts

    import_signatures 

    import_avatars

    sticky_forum_topics

    close_forum_topics

    create_permalinks

    SiteSetting.newuser_spam_host_threshold = 3
    #SiteSetting.body_min_entropy = 7
    #SiteSetting.min_topic_title_length = 15
    #SiteSetting.title_min_entropy = 10
    SiteSetting.allow_uppercase_posts = false
  end

  def import_users
    puts '', "Importing users"

    query =
      "SELECT count(*) as count
       FROM #{TABLE_PREFIX}_users;"
    total_count = @client.query(query).first['count']
    puts "Total count: #{total_count}"
    @last_user_id = -1

    batches(BATCH_SIZE) do |offset|
      query = "SELECT *
      FROM #{TABLE_PREFIX}_users
      WHERE user_id > #{@last_user_id}
      LIMIT #{BATCH_SIZE};"

      results = @client.query(query)
      break if results.size < 1
      @last_user_id = results.to_a.last['user_id']

      create_users(results, total: total_count, offset: offset) do |user|
        #puts '', user['user_id'].to_s + ' ' + user['displayname']
        trust_level = user['approved'] == 1 ? 3 : 0
        displayname = user['displayname'].blank? ? user['username'] : user['displayname']
        { id: user['user_id'],
          email: user['email'],
          username: user['username'],
          name: displayname,
          created_at: user['creation_date'],
          active: true,
          last_seen_at: user['lastlogin_date'],
          trust_level: trust_level,
          approved: user['approved'] == 1,
          send_welcome_message: false,
          skip_email_validation: true,
          suspended_at: (user['approved'] == 1 ? nil : Date.today-1.day)
        }
      end
    end
  end

  def add_deleted_users
    puts '', "Adding deleted users"
    # => If you have users that have been deleted, you will need a placeholder
    # => for that user in Discourse if they have posted any content

    user_ids = [...]
    create_users(user_ids) do |user_id|
      {
        id: user_id,
        email: "#{SecureRandom.hex}@anon.#{Discourse.current_hostname}",
        name: "Deleted_User_#{user_id}",
        username: "Deleted_User_#{user_id}",
        created_at: 1.day.ago,
        approved_at: 1.day.ago,
        approved: true,
        active: false,
        trust_level: 1,
        manual_locked_trust_level: 1,
        send_welcome_message: false,
        skip_email_validation: true
      }
    end
  end

  def disable_users
    puts "", "Disabling users..."

    count_query =
      "SELECT count(*) count
      FROM #{TABLE_PREFIX}_users
      where enabled=0 or verified=0 or approved=0"
    total_count = @client.query(count_query).first['count']
    puts "Total count: #{total_count}"

    query = "SELECT
      *
      FROM #{TABLE_PREFIX}_users
      where enabled=0 or verified=0 or approved=0"

    users_disabled = 0
    start_time = Time.now

    users = @client.query(query)
    users.each do |u|
      user_id = user_id_from_imported_user_id(u['user_id'])
      user = User.find(user_id)

      user.update(
        trust_level: 0,
        approved: false,
        active: false,
        suspended_at: Date.today - 1.day,
        suspended_till: Date.today + 50.years
      )
      users_disabled += 1
      print_status(users_disabled, total_count, start_time)
    end    
  end

  def import_categories
    puts "", "Importing Categories..."

    query = "SELECT * from #{TABLE_PREFIX}_forum_forums"
    results = @client.query(query)

    create_categories(results) do |c|
      {
        id: c["forum_id"],
        name: c["title"],
        description: c["description"],
        parent_category_id: c["parent_forum_id"]
      }
    end
  end

  def import_topics_and_posts
    puts "", "Importing Topics and Posts..."

    count_query =
      "SELECT count(*) count
       FROM #{TABLE_PREFIX}_forum_topics;"
    total_topic_count = @client.query(count_query).first['count']
    puts "Total Topic count: #{total_topic_count}"

    count_query =
      "SELECT count(*) count
       FROM #{TABLE_PREFIX}_forum_posts;"
    total_count = @client.query(count_query).first['count']
    puts "Total Post count: #{total_count}"

    @last_topic_id = -1

    query = "SELECT
      *
      FROM #{TABLE_PREFIX}_forum_topics
      ORDER BY topic_id"
      # => If you consistenlty have timeouts, you may need to
      # => import in blocks using this format:
      # => ORDER BY topic_id LIMIT 2500 OFFSET 2499"
    topics = @client.query(query)
    topics.each do |t|
      forum_id = t['forum_id']
      forum_id = 3 if forum_id == 4
      category_id = category_id_from_imported_category_id(forum_id)
      category = Category.find_by(id: category_id)
      category = Category.find(6) if category.nil?  # => Substitute for your General Category

      post_query = "SELECT
        *
        FROM #{TABLE_PREFIX}_forum_posts
        WHERE topic_id = #{t['topic_id']}
        order by post_id;"
      posts = begin
        @client.query(post_query)
      rescue Exception => e
        @client.query(post_query)
      end

      first_post = posts.first

      if first_post.nil?
        pp "Topic #{t['topic_id']} has no posts attached"
        next
      end

      user_id = user_id_from_imported_user_id(first_post['user_id'])
      if user_id.nil?
        pp "UserID #{first_post['user_id']} not found"
        pp t.inspect
        raise "UserID #{first_post['user_id']} not found"
      end
      user = User.find(user_id)
      raise if user.nil?

      # => Does topic exist yet?
      topic = TopicCustomField.find_by(name: 'import_id', value: t['topic_id'])&.topic

      if topic.nil?
        # => Some bodies are \r\n markdown, some are HTML
        first_post_body = body_as_markdown(first_post['body'])
        # => Fine-tune this as needed
        if first_post_body == "\r\n" || 
          first_post_body == "?" || 
          first_post_body == "." || 
          first_post_body == ":)" ||
          first_post_body.gsub("?","").length < 2 ||
          first_post_body.gsub(".","").length < 2
          first_post_body = t['title']
        end
        first_post_body = first_post_body.gsub("www.rhb-grischun.ca", "")

        suspended_user = false
        if user.suspended?
          suspended_user = true
          user.update_column(:suspended_till, nil)
        end
        topic_post = PostCreator.create!(user, {
          title: t['title'],
          category: category.id,
          raw: first_post_body,
          created_at: t['creation_date'],
          views: t['view_count'],
          closed: t['closed'] == 1,
          posts_count: t['post_count'],
          import_mode: true,
          skip_validations: true,
          skip_guardian: true
        })
        topic = topic_post.topic
        TopicCustomField.create!(topic_id: topic.id, name: 'import_id', value: t['topic_id'])
        PostCustomField.create!(post_id: topic_post.id, name: 'import_id', value: first_post['post_id'])
        if suspended_user
          user.update_column(:suspended_till, "2022-12-25")
        end
      end
      first_post_on_topic = topic.first_post
      # => Now the rest of the posts in the thread
      posts.drop(1).each do |p|
        user_id = user_id_from_imported_user_id(p['user_id'])
        if user_id.nil?
          pp "UserID #{p['user_id']} not found"
          pp t.inspect
          raise "UserID #{p['user_id']} not found"
        end
        user = User.find(user_id)
        raise if user.nil?
        # => Does post exist yet?
        next if PostCustomField.find_by(name: 'import_id', value: p['post_id'])

        post_body = body_as_markdown(p['body'])
        # => post-process the post_body here if necessary 
        if post_body == "\r\n"
          post_body = "<post missing>"
        end
        if post_body == ":)" || post_body == ";-)"
          post_body = "...grinning..."
        end
        if post_body == ";)"
          post_body = "...winking..."
        end
        # => etc...
        suspended_user = false
        if user.suspended?
          #Mon, 25 Dec 2220 20:10:12 UTC +00:00
          suspended_user = true
          user.update_column(:suspended_till, nil)
        end

        post = PostCreator.create!(user, {
          topic_id: topic.id,
          reply_to_post_number: first_post_on_topic.id, 
          raw: post_body,
          created_at: p['creation_date'],
          import_mode: true,
          skip_validations: true,
          skip_guardian: true
        })
        PostCustomField.create!(post_id: post.id, name: 'import_id', value: p['post_id'])
        if suspended_user
          user.update_column(:suspended_till, "2022-12-25")
        end

      end
    end
  end

  # => Mark topics as sticky
  def sticky_forum_topics
    puts "", "Pinning Topics..."

    count_query =
      "SELECT count(*) count
       FROM #{TABLE_PREFIX}_forum_topics
       where sticky=1;"
    total_count = @client.query(count_query).first['count']
    puts "Total count: #{total_count}"

    query = "SELECT
      *
      FROM #{TABLE_PREFIX}_forum_topics
      where sticky=1;"

    topics_pinned = 0
    start_time = Time.now

    topics = @client.query(query)
    topics.each do |t|
      # => Does topic exist yet?
      topic = TopicCustomField.find_by(name: 'import_id', value: t['topic_id'])&.topic

      if topic.nil?
        raise "Topic not found!"
      end
      topic.update(pinned_at: topic.created_at)
      topics_pinned += 1
      print_status(topics_pinned, total_count, start_time)
    end
  end

  # => Mark topics as closed
  def close_forum_topics
    puts "", "Closing Topics..."

    count_query =
      "SELECT count(*) count
       FROM #{TABLE_PREFIX}_forum_topics
       where closed=1;"
    total_count = @client.query(count_query).first['count']
    puts "Total count: #{total_count}"

    query = "SELECT
      *
      FROM #{TABLE_PREFIX}_forum_topics
      where closed=1;"

    topics_closed = 0
    start_time = Time.now

    topics = @client.query(query)
    topics.each do |t|
      # => Does topic exist yet?
      topic = TopicCustomField.find_by(name: 'import_id', value: t['topic_id'])&.topic

      if topic.nil?
        raise "Topic not found!"
      end
      topic.update(closed: true)
      topics_closed += 1
      print_status(topics_closed, total_count, start_time)
    end
  end

  # => User.custom_fields:
  # => "signature_raw"=>"This is my sig\n\nLa la la",
  # => "signature_cooked"=>"<p>This is my sig</p>\n<p>La la la</p>"}

  def import_signatures
    puts "", "Importing Signatures..."

    count_query =
      "SELECT count(*) count
       FROM #{TABLE_PREFIX}_forum_signatures
       WHERE signature is not null"
    total_count = @client.query(count_query).first['count']
    puts "Total count: #{total_count}"

    signatures_imported = 0
    start_time = Time.now

    query =
      "SELECT *
       FROM #{TABLE_PREFIX}_forum_signatures
       WHERE signature is not null"
    signatures = @client.query(query)

    signatures.each do |sig|
      next if sig['signature'].nil?
      user_id = @lookup.user_id_from_imported_user_id(sig['user_id'])
      next if user_id.nil?
      user = User.find_by(id: user_id)
      print_status(signatures_imported += 1, total_count, start_time)
      # => Do we already have this?
      ucf = UserCustomField.find_by(name: "signature_raw", user_id: user.id)
      next unless ucf.nil?

      sig_cooked = sig['signature']
      sig_raw = sig_cooked
      UserCustomField.create!(user_id: user.id, name: "signature_raw", value: sig_raw)
      UserCustomField.create!(user_id: user.id, name: "signature_cooked", value: sig_cooked)        
    end
  end

  def import_avatars
    puts "", "Importing user avatars"

    query =
      "SELECT count(*) count
      FROM #{TABLE_PREFIX}_users 
      where photo_id != 0 and 
      lastlogin_date is not null and 
      lastlogin_date > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 5 YEAR)"
    total_count = @client.query(query).first['count']
    puts "Total count: #{total_count}"
    created = skipped = none = 0
    start_time = Time.now

    query = 
      "SELECT * FROM #{TABLE_PREFIX}_users 
      where photo_id != 0 and 
      lastlogin_date is not null and 
      lastlogin_date > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 5 YEAR) 
      order by lastlogin_date desc"

    users = @client.query(query)

    users.each do |u|
      begin
        user_id = user_id_from_imported_user_id(u['user_id'])
        user = User.find(user_id)
        if user
          if user.has_uploaded_avatar
            skipped += 1
            next
          end

          Rails.logger.info("SE User #{u['user_id']} is Discourse User #{user.id}")
          avatar_url = "https://www.yourolddomain.com/profile/#{u['user_id']}"
          html_body = HTTParty.get(avatar_url)
          # => Find IMG with a class thumb_profile, then grab the URL 
          page = Nokogiri::HTML(html_body)
          content = page.at('.thumb_profile')
          if content.nil?
            none += 1
            next
          end 
          image_path = content.attributes['src'].value.split("?")[0]
          # => Skip if no avatar file
          if image_path.include?("nophoto_user_thumb_profile")
            none += 1
            next 
          end

          filename = "avatar-#{user_id}.png"
          path = File.join(AVATAR_DIR, filename)
          File.open(path, 'wb') { |f|
            f << open("https://www.yourolddomain.com#{image_path}").read
          }

          upload = @uploader.create_upload(user.id, path, filename)

          if upload.persisted?
            user.import_mode = false
            user.create_user_avatar
            user.import_mode = true
            user.user_avatar.update(custom_upload_id: upload.id)
            user.update(uploaded_avatar_id: upload.id)
            created += 1
          else
            Rails.logger.error("Could not persist avatar for user #{user.username}")
          end
          print_status(created + skipped + none, total_count, start_time)
        end
      rescue ActiveRecord::RecordNotFound
        Rails.logger.error("Could not find User for user_id: #{a['c_user']}")
      end
    end
  end

  def fix_date_from_se(db_date)
    db_date.to_time.to_i
  end

  def body_as_markdown(body)
    if body_is_already_markdown?(body)
      return body
    #elsif looks_like_bbcode(body)
    #  return body.bbcode_to_md 
    else
      return HtmlToMarkdown.new(body).to_markdown
    end
  end

  def body_is_already_markdown?(string_to_test)
    !string_to_test.include?("<p>")
  end

  def looks_like_bbcode(string_to_test)
  end

  def create_permalinks
    puts '', 'Creating redirects...', ''

    # => Categories/Forums:
    Category.find_each do |cat|
      ccf = cat.custom_fields
      next unless orig_id = ccf["import_id"]
      slug = cat['slug']
      Permalink.create(url: "/forums/#{orig_id}", category_id: cat.id) rescue nil
      Permalink.create(url: "/forums/#{orig_id}/#{slug}", category_id: cat.id) rescue nil
    end

    # => Topics
    Topic.listable_topics.each do |topic|
      tcf = topic.custom_fields
      if tcf && tcf["import_id"]
        orig_id = tcf["import_id"]
        slug = topic.slug
        Permalink.create(url: "/forums/topic/#{orig_id}/#{slug}", topic_id: topic.id) rescue nil
      end
    end
  end

  def staff_guardian
    @_staff_guardian ||= Guardian.new(Discourse.system_user)
  end

end

ImportScripts::SocialEngine.new.perform
