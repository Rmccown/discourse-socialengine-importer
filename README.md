# Discourse SocialEngine Importer

A rough importer to import SocialEngine into Discourse.  Probably will not work for you without changes.  Consider this project a starting point for you.

I have been running an SE forum (4.8.6?) for about 10 years, and other packages before that.  I have ~5000 users and ~400k posts.  I wanted to bring this forum over to Discourse with a minimum of fuss.  The script here is a complete hack, and definitely not ready to run on your forum.  I had the luxury of spinning up Discourse on a second domain, testing the importer as I worked on it, and still kept the main site up.  I ran into mysql timeouts and had to import in 2500-thread chunks.  YMMV for all of this.  I will try to answer any questions you may have. If someone wants to take a stab at making this an official importer, I will welcome pull requests

# Prerequisites

You (obvously) need a working Discourse installation.  Your SocialEngine site should be set to allow external MYSQL access.  You need ssh/sftp access to your Discourse server.

# Usage


SFTP to your Discourse server, and put the files into the a shared directory.
```
$ sftp root@<your server>
sftp> cd /var/discourse/shared/standalone/import
sftp> put socialengine.rb
sftp> exit
```
SSH to your Discourse server and copy the import script into the container
```
$ ssh root@<your server>
root@xxx:~# cd /var/discourse
root@xxx:/var/discourse# ./launcher enter app
root@xxx-app:/var/www/discourse# cd /var/www/discourse
root@xxx-app:/var/www/discourse# cp /shared/import/socialengine.rb script/import_scripts/
```
Hack the Discourse gemfile to add needed gems

```
root@xxx-app:/var/www/discourse# cd /var/www/discourse
root@xxx-app:/var/www/discourse# echo "gem 'mysql2', require: false" >> Gemfile
root@xxx-app:/var/www/discourse# echo "gem 'httparty', require: false" >> Gemfile
root@xxx-app:/var/www/discourse# apt-get update
root@xxx-app:/var/www/discourse# sudo apt-get install libmariadb-dev
root@xxx-app:/var/www/discourse# bundle config unset deployment
root@xxx-app:/var/www/discourse# bundle install
```

Run the importer

```
root@xxx-app:/var/www/discourse# cd /var/www/discourse
root@xxx-app:/var/www/discourse# su discourse -c 'bundle exec ruby script/import_scripts/socialengine.rb'
```
